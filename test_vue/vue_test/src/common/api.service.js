import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
// import JwtService from "@/common/jwt.service";
import { API_URL } from "@/common/config";

const ApiService = {
  init() {
    Vue.use(VueAxios, axios);
    Vue.axios.defaults.baseURL = API_URL;
    
  },

  setHeader(resource) {
    Vue.axios.defaults.headers.common[
      "Authorization"
    //  ] = `Token ${JwtService.getToken()}`;
      ] = `Token ${resource}`;
  },

  query(resource, params) {
    return Vue.axios.get(resource, params).catch(error => {
      throw new Error(`[RWV] ApiService ${error}`);
    });
  },


  get(resource) {
    return Vue.axios.get(resource).catch(error => {
      throw new Error(`[RWV] ApiService ${error}`);
    });
  },
  getUser(resource, user) {
    return Vue.axios.get(`${resource}/${user}`).catch(error => {
      throw new Error(`[RWV] ApiService ${error}`);
    });
  },

  post(resource, params) {
    return Vue.axios.post(`${resource}`, params);
  },

  update(resource, slug, params) {
    return Vue.axios.put(`${resource}/${slug}`, params);
  },

  put(resource, params) {
    return Vue.axios.put(`${resource}`, params);
  },

  delete(resource) {
    return Vue.axios.delete(resource).catch(error => {
      throw new Error(`[RWV] ApiService ${error}`);
    });
  }
};

export default ApiService;






export const ProfileService = {

  get(userdata){
    return ApiService.getUser("profile/profiles", userdata)   
 },

  updateProfile(data){
    return ApiService.put("api/user/", {
      "user":data
    })
  }
}

export const CustomCodeService = {
    query(params){
      
        ApiService.query("customcode",{
          params:params
        });
    },
    get(){
            
      return ApiService.get("customcode/")
    }
}