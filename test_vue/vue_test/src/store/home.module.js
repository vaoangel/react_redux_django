import {CustomCodeService} from '@/common/api.service';
import {GET_CUSTOM_COMPS} from "./actions.type"
import {
    FETCH_START,
    FETCH_END,
   
  } from "./mutations.type";
const state ={
    customcode:[],
    isLoading: true,
    customcodeCount: 0
}


const getters = {
    customcodeCount(state){
        return state.customcodeCount;
    },
    customcode(state){
        return state.customcode
    },
    isLoading(state) {
        return state.isLoading;
      },
}



const actions = {
    [GET_CUSTOM_COMPS]({commit}){

        commit(FETCH_START);
        

        return CustomCodeService.get()
        .then(({data})=>{
          
            commit(FETCH_END, data)
        })
        .catch(error => {
            throw new Error(error);
          });
    }
}
const mutations = {
    [FETCH_START](state) {
      state.isLoading = true;
    },
    [FETCH_END](state, { customcode, customcodeCount }) {
      state.customcode = customcode;
      state.customcodeCount = customcodeCount;
      state.isLoading = false;
    },
    // [SET_TAGS](state, tags) {
    //   state.tags = tags;
    // },
    // [UPDATE_ARTICLE_IN_LIST](state, data) {
    //   state.articles = state.articles.map(article => {
    //     if (article.slug !== data.slug) {
    //       return article;
    //     }
    //     // We could just return data, but it seems dangerous to
    //     // mix the results of different api calls, so we
    //     // protect ourselves by copying the information.
    //     article.favorited = data.favorited;
    //     article.favoritesCount = data.favoritesCount;
    //     return article;
    //   });
    // }
  };

export default {
    state,
    getters,
    actions,
    mutations
}