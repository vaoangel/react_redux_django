import {ProfileService} from '@/common/api.service';
import{GET_PROFILE,UPDATE_PROFILE} from "./actions.type"
import {SET_PROFILE} from './mutations.type'


  const state ={
    user:{},
}


const getters = {
  
    user(state){
        return state.user
    },
   
}


const actions = {
  [GET_PROFILE](context,payload) {
    const { username } = payload;
    const {context2} = context;
     ProfileService.get(username,context2).then(
      data =>{
        context.commit(SET_PROFILE, data);
          return data
    }
    )
     
  },

  [UPDATE_PROFILE](context,payload){
    ProfileService.updateProfile(payload).then(
      data =>{
        context.commit(SET_PROFILE, data);
          return data
      }
    )
  }


}


const mutations = {
  [SET_PROFILE](state, user) {
    state.user = user.data.user;
    state.errors = {};
  }
}



export default {
    state,
    getters,
    actions,
    mutations
}