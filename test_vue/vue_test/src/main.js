import Vue from 'vue'
import App from './App.vue'
import router from "./router";
import store from "./store";
import ApiService from "./common/api.service";
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
Vue.config.productionTip = false
ApiService.init();

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
