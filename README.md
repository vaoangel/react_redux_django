# React-Redux-Django Demo (With Vue simple Example)

Este proyecto recrea la funcionalidad de una red social en la que sus usuarios crean y comparten componentes estilizados
desde la propia aplicación.

### Pre-requisitos 📋
NodeJs para utilizar npm o en otro orden de cosas sustituyelo por el gestor que prefieras.
Docker y docker-compose para poder instalar nuestro Backend:
* [Docker en ubuntu](https://www.digitalocean.com/community/tutorials/como-instalar-y-usar-docker-en-ubuntu-18-04-1-es)
* [Docker en Windows](https://docs.docker.com/docker-for-windows/install/)
* [Docker-Compose](https://www.digitalocean.com/community/tutorials/como-instalar-docker-compose-en-ubuntu-18-04-es)

### Instalación 🔧
Antes de todo clonaremos el repositorio. Ya clonado instalaremos los apartados en el siguiente orden:
* Backend
```
cd backend
docker-compose up --build

(Abrimos una consola del contenedor creado)
python manage.py makemigrations
python manage.py migrate
```
* Frontend React
```
cd frontend
npm install // yarn install
(Ya instalado)
npm run start // yarn serve
```
* Fronend Vue 
```
cd test_vue
npm install // yarn install
(Ya instalado)
npm run start // yarn serve
```

## Construido con 🛠️
* [React](https://es.reactjs.org/) - El framework web usado
    * [Redux](https://es.redux.js.org/) 
    * [Hooks](https://es.reactjs.org/docs/hooks-intro.html)
* [Vue](https://vuejs.org/) - El framework web usado
    * [Vuex](https://vuex.vuejs.org/)
* [Django](https://www.djangoproject.com/) - Tecnologia de backend
    * [Django-rest-framework](https://www.django-rest-framework.org/)
* [Postgres](https://www.postgresql.org/) - Tecnologia de base de datos

## Wiki 📖

Puedes encontrar mucho más de cómo utilizar este proyecto:
1. [Backend](media_readme/backend_README.MD)
2. [Frontend React](media_readme/react_readme/react.md)
3. [Vue](media_readme/vue_readme/README.md)



