# VUE small app


## Funcionalidad

* GetAll
* Get profile
* Update Profile


`Aqui tenemos un list proveniente de una petición al servidor`

![alt text](./media_readme/GetAllVue.png)


`Principalmente la peticion empieza en el store del módulo home mediante el atributo action y si la petición ha ido bien nos da paso a guardar la información en el state para que sea visible en el componente de vue`

![alt text](./media_readme/HomeExp.png)



`Seguidamente se recoje la información con el mapGetters en el componente y se pinta dentro de la etiqueta template`

![alt text](./media_readme/HomeVue.png)
