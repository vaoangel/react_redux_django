[Atras](../../README.md)
# **React-Redux FrontEnd**
El frontend de esta aplicación ha sido creado con React intentando explotar al maximo las nuevas funcionalidades del ciclo de vida de los componentes, usando equitativamente componentes funcionales, de alto orden y clases, incrmentando su escalabilidad añadiendole React-Redux y Hooks. En adición a todo lo anterior nombrado, ciertos componentes estan programados con reactCreateElement.

- [Router & Agent](react_router.md) 🚀
- [Redux](redux.md) 📖
- [Components](components.md) 🛠️


<img src="../preview.gif"  width="700" height="400" style="  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;" />
