# **React-Redux Components**
Las clases componente de react tienen sus fases de vida las cuales se efectuan antes y despues de que el render 
construya el componente. Desde React 16 el ciclo de vida de estos componente ha cambiado haciendo inseguros fases del ciclo de
vida muy utilizadas:
* componentWillMount → UNSAFE_componentWillMount
* componentWillReceiveProps → UNSAFE_componentWillReceiveProps
* componentWillUpdate → UNSAFE_componentWillUpdate

Desde la salida de react 16.9 estas fases del ciclo de vida han sido remplazadas por nuevos metodos que intentan suplir estas
funcionalidades y añaden nuevas:
* static getDerivedStateFromProps
* getSnapshotBeforeUpdate

En este repositorio se usan las alternativas dadas a estos componentes inseguros.

## **Mejoras respecto al ciclo de vida**

### **Constructor y shouldComponentUpdate**
En el caso de querer realizar una llamada antes de que el componente se monte, en versiones posteriores a react 16, usariamos
componentWillMount.

En estas nuevas versiones las llamadas asincronas las realizaremos desde el constructor.

Otro punto a tener en cuenta es si queremos evitar renderizaciones innecesarias. En ese caso usaremos shouldComponentUpdate.
Un ejemplo practico visto en este repositorio es el caso de re-renderizar los componentes tras unas redirección posterior a un 
loguin correcto:

```
shouldComponentUpdate(nextProps, nextState) {
    if(nextProps.redirectTest !== this.props.redirectTest && nextProps.redirectTest !== null){
        this.props.unloadLogin(this.props.history.push(nextProps.redirectTest))
        return true
    }
    return false
}
```
### **static getDerivedStateFromProps**
En este caso usamos este metodo para sustituir componentWilReciveProps. Obtenemos de los props que conseguiremos desde
el servidor y crearemos un estado con los props que hemos recibido. Permitimos a un componente actualizar su estado interno como resultado de cambios en las props sin una renderización innecesaria.
```
    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.component !== prevState.component) {
            return {
                component:nextProps.component.customcode,
                id: nextProps.component.customcode.component,
                style:nextProps.component.customcode.code
            };
          }
      
          // Return null if the state hasn't changed
          return null;
      }
```
### **getSnapshotBeforeUpdate**
componentWillUpdate se usa para leer las propiedades que actualizan el DOM. Sin embargo, con el renderizado asíncrono, puede haber retrasos entre los ciclos de vida de la fase de "render" y los ciclos de vida de la fase de "commit". Si el usuario hace algo como obtener datos del servidro durante este tiempo, el valor que contendrá los datos, leído desde componentWillUpdate, será "undefined" y renderizara con este contenido.

La solución a este problema es utilizar el nuevo metodo del ciclo de vida de la fase de "commit", getSnapshotBeforeUpdate. Este método se llama inmediatamente antes de que se realicen las mutaciones (por ejemplo, antes de que se actualice el DOM). Puede devolver un valor para que React pase como parámetro a componentDidUpdate, que se llama inmediatamente después de las mutaciones.

```
getSnapshotBeforeUpdate(prevProps, prevState) {
    if (prevProps.list_components != this.props.list_components) {
        const snapshot = this.props.list_components;
        return snapshot
    }    
    return null;
}
componentDidUpdate(prevProps, prevState, snapshot) {
    if(snapshot != null){
        this.setState({
            components: snapshot.customcode
        })
        this.props.success()
    }
    
}
```

## **Hooks y React.CreateElement**
Otro de los puntos a tener en cuenta de esta aplicación es el uso de Hooks y HipperScript.

Hooks ha sido usado para crear componentes funcionales que no necesitan de un ciclo de vida o que dependen de un componente que ya cuenta con uno.

Una mejora significativa en la usabilidad de los componentes presentacionales ha sido el uso de React.createElement el cual nos ha permitido crear un componente super customizable y reutilizable.

```
export const CustomizableButton = (props) =>{
    const e = React.createElement; /* elemento html */

    /* CSS del modo diurno y nocturno. Carga mediante Hooks el state proporcionado por el reducer
       de estilos. Pasado por el props.component_type selectionamos del objeto style porporcionado por el reducer
       un estilo para nestro boton*/
    let customWeather = useSelector(state => state.StyleReducer.style[props.component_type])
    /* si existe un props.component_sub_type, seleccionaremos del objeto obtenido por props.component_type un estilo de
    su interior */
    if(props.component_sub_type !== undefined){
        customWeather = customWeather[props.component_sub_type]
    }

    /* Creamos el objeto de los props del elemento. Por defecto el objeto solo tendrá estilo */
    let custom_props = {style:customWeather}
    /* En el caso de que por props pasemos un evento al componente lo añadiremos al objeto */
    if(props.event!==undefined){
        custom_props[props.event.type]=props.event.dispatch;

    }

    return (
        e("button",custom_props,props.text)
    )
}


```

## **Componentes Destacables de la aplicación**
Los tres componentes mas destacables de la aplicación formarian parte de un unico proposito: crear la funcionalidad de hacer y ver los componentes personalizados.

* [Component Creator](frontend/src/components/Component.Creator.component.js)
* [Component Detail Container](frontend/src/components/styler_components/Detail_Created_Component.component.js)
* [Component Detail](frontend/src/components/styler_components/Detail_Component.component.js)

