[Atras](../../README.md)
- [Router & Agent](react_router.md) 🚀
- [Components](Authentication.md) 🛠️

# **Redux**
La arquitectura del modulo Redux es la estandar establecida por la comunidad a excepción de lo que veremos en este documento:

## **Store**
Nuestro store esta vinculado a los [webtools](https://github.com/zalmoxisus/redux-devtools-extension) del navegador para poder usar las extensiones pertinentes y asi facilitar el debug de la aplicación:

```
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducer, /* preloadedState, */ composeEnhancers(
    applyMiddleware(promiseMiddleware)
  ));
```
## **Reducers**
Los reducers tienen dos cosas a destacar. Todos los reducers de la aplicación estan juntos en el [conbineReducers()](https://es.redux.js.org/docs/api/combine-reducers.html)

La mayoría de los reducers programados en esta app tienen la peculiaridad de separar la acción de modificar el state de store.
Ejemplo:
```
const logout =(state,action)=>{
    return { ...state, 
                token: null,
                currentUser: null,
                FromLoginTo:'/' };
}
const Action = ActionTypes.AUTH_TYPES
export default (state = defaultState, action)=>{
    switch (action.type) {
        ...
        case Action.LOGOUT:
            return logout(state,action)
        ...
    }
}
```

Otra de las peculiaridades programadas en estos reducers es el tratamiento de los datos en el mismo state del reducer:
```
const defaultState ={
    FromLoginTo:null,
    token: window.localStorage.getItem('jwt')!==null ? window.localStorage.getItem('jwt') : null,
    currentUser: window.localStorage.getItem('user')!==null ? JSON.parse(window.localStorage.getItem('user')) : null,
    loading:false,
    error:null,
}
```