[Atras](../../README.md)
- [Redux](contact.md) 📖
- [Components](Authentication.md) 🛠️
# **React-Redux Router**
Encargado de organizar todas las rutas de la aplicación en tres tipos:
* Publicas
* Privadas
* Restringidas

## **Rutas publicas**
Las rutas publicas de la aplicación seran todas aquellas rutas que todo tipo de usuario pueda manejar.
```
<Route exact path='(ruta)' component={(Componente)}/>
```

## **Privadas**
Son las rutas que tan solo los usuarios loguedos pueden manejar.
```
<CustomRoutes options={{elementType:"private",redirect:"(ruta)"}} component={(componente)} exact path="(ruta)"  />
```

## **Restringidas**
Son las rutas que no son accesibles al usuario por motivos de seguridad o integridad de codigo.
```
<CustomRoutes options={{elementType:"restricted",redirect:"(ruta)"}} component={(componente)} exact path="(ruta)"  />
```

## **Logica tras las rutas**
Usaremos el componente Route de React-router-dom para las rutas publicas y un componente funcional, llamado
**CustomRoutes** el cual nos servirá para determinar que tipo de ruta vamos a crear:
<img src="./media/router1.png" 
            style="  display: block;
            margin-left: auto;
            margin-right: auto;
            width: 50%;" />
Tal y como se ve en la imagen anterior este componente usa [Hooks](https://es.reactjs.org/docs/hooks-intro.html) con el 
cual obtenemos del state del store si existe un usuario logueado.

# **Axios Agent**

El Agent usado en esta aplicación esta creado sobre la libreria Axios. Este esta dividido en tres: Base, Request, Api.

## **Base**
Es el encargado de crear la url base de todas las peticiones y setear las cabeceras de autentificación.

## **Request**
Es el encargado de mantener las peticiones basicas (Get, Put, Post,etc)

## **Api**
Es el encargado de montar los diferentes tipos logica que la aplicación puede demandar del servidor:
```
const AuthApi ={
    ....
    getProfile: (username) =>
                request.get('profile/profiles/'+username),
    createComponent :(component)=>
                request.post('customcode/',{customcode:{code:JSON.stringify(component.style),component:component.type}}),
    ....
}
```
