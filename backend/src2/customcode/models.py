from django.db import models



class CustomCode(models.Model):


    component = models.TextField(blank=True)
    code = models.TextField(blank=True)
    user = models.ForeignKey(
        'profiles.Profile', on_delete=models.CASCADE, related_name='customcode'
    )


    def __str__(self):
        return self.user.email

class Comments(models.Model):
    body = models.TextField(max_length=255)

    component = models.ForeignKey(
        'customcode.CustomCode', related_name='comments', on_delete=models.CASCADE
    )

    user = models.ForeignKey(
        'profiles.Profile', on_delete=models.CASCADE, related_name='comments'
    )