from django.shortcuts import render
from rest_framework import generics, mixins, status, viewsets
from rest_framework.exceptions import NotFound
from rest_framework.permissions import (
    AllowAny, IsAuthenticated, IsAuthenticatedOrReadOnly, IsAdminUser
)
from .renderers import CustomCodeJSONRenderer, CommentJSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import CustomCode, Comments
from .serializers import CustomCodeSerializer,CommentSerializer
from django.shortcuts import get_list_or_404, get_object_or_404

 
class CustomCodeViewSet(mixins.CreateModelMixin,mixins.ListModelMixin,viewsets.GenericViewSet, mixins.DestroyModelMixin):
    queryset = CustomCode.objects.select_related('user', )
    permission_classes = (AllowAny,)
    renderer_classes = (CustomCodeJSONRenderer,)
    serializer_class = CustomCodeSerializer
    def retrieve(self, request, *args, **kwargs):
        serializer_data = request.data.get("customcode", {})
        try:
            customcode = self.queryset.get(id=serializer_data['id'])
        except CustomCode.DoesNotExist:
            raise NotFound('A profile with this username does not exist.')

        serializer = self.serializer_class(customcode, context={
            'request': request
        })

        return Response(serializer.data, status=status.HTTP_200_OK)
    def create(self, request):
        serializer_context = {
            'user': request.user.profile,
            'request': request
        }
        serializer_data = request.data.get("customcode", {})
        serializer = self.serializer_class(
        data=serializer_data, context=serializer_context
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_201_CREATED)      

    def list(self, request):
        serializer_context = {'request': request}
        page = self.paginate_queryset(self.get_queryset())

        serializer = self.serializer_class(
            page,
            context=serializer_context,
            many=True
        )
        return self.get_paginated_response(serializer.data)
    def destroy(self, request):
        serializer_data = request.data.get("customcode", {})
        try:
            customcode = self.queryset.get(id=serializer_data['id'])
      
        except CustomCode.DoesNotExist:
            raise NotFound('An customcode with this slug does not exist.')
        customcode.delete()
        return Response(serializer_data ,status=status.HTTP_200_OK)
  


class CommentsViewSet(mixins.CreateModelMixin,mixins.ListModelMixin,viewsets.GenericViewSet, mixins.DestroyModelMixin):
    queryset = CustomCode.objects.select_related('user')
    permission_classes = (IsAuthenticatedOrReadOnly,)
    renderer_classes = (CommentJSONRenderer,)
    serializer_class = CommentSerializer

    def create(self, request):
        serializer_context = {
            'user': request.user.profile,
            'request': request
        }

        serializer_data = request.data.get("comment", {})
        serializer = self.serializer_class(
        data=serializer_data, context=serializer_context
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_201_CREATED)  
  

    def list(self, request):
        serializer_context = {'request': request}
        page = self.paginate_queryset(self.get_queryset())

        serializer = self.serializer_class(
            page,
            context=serializer_context,
            many=True
        )
        return self.get_paginated_response(serializer.data)


    def retrieve(self, request, *args, **kwargs):
        serializer_data = request.data.get("comments", {})
        print("dasdsadasssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss")
        try:
            customcode = self.queryset.get(id=serializer_data['id'])
        except CustomCode.DoesNotExist:
            raise NotFound('A profile with this username does not exist.')

        serializer = self.serializer_class(customcode, context={
            'request': request
        })

        return Response(serializer.data, status=status.HTTP_200_OK)