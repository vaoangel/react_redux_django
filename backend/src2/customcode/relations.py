from .models import Comments

from rest_framework import serializers


class CommentsRelated(serializers.RelatedField):
    def get_queryset(self):
        return Comments.objects.all()

    def to_representation(self, value):
        return value.comment