from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter
from .views import CustomCodeViewSet, CommentsViewSet

app_name = 'customcode'

router = DefaultRouter()

router.register(r'customcode',CustomCodeViewSet)
router.register(r'comments',CommentsViewSet)

customcode_list= CustomCodeViewSet.as_view({
    'delete': 'destroy',
    'post': 'create',
    'get': 'list',
    'patch': 'retrieve'


}) 

comments_list= CommentsViewSet.as_view({
    'delete': 'destroy',
    'post': 'create',
    'get': 'list',


})

urlpatterns = [
      url('', include(router.urls)),
      url('', customcode_list),
      url('', comments_list)



]