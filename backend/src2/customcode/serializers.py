from rest_framework import serializers
from .models import CustomCode, Comments
from profiles.serializers import ProfileSerializer


class CustomCodeSerializer(serializers.ModelSerializer):
    user = ProfileSerializer(read_only=True)
    component = serializers.CharField(required=True)
    code = serializers.CharField(required=True)


    class Meta:
        model = CustomCode
        fields = (
            'id',
            'user',
            'component',
            'code',
            'comments'

        )
        read_only_fields = ('id','comments')

    def create(self, validated_data):
        user = self.context.get('user', None)
        Comments= self.context.get('comments',None)
        customcode = CustomCode.objects.create(user=user, **validated_data)

        return customcode



    def get_favorited(self, instance):
        request = self.context.get('request', None)

        if request is None:
            return False

        if not request.user.is_authenticated():
            return False
 

class CommentSerializer(serializers.ModelSerializer):

    body = serializers.CharField(read_only=False)

    user = ProfileSerializer(read_only=True)

    class Meta:
        model = Comments
        fields = (
            'user', 
            'body',
            'component'
        )

    def create(self, validated_data):
        user = self.context['user']

        return Comments.objects.create(
            user=user ,  **validated_data
        )
