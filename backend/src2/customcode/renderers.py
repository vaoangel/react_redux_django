from src2.renderers import CustomJSONRenderer

class CustomCodeJSONRenderer(CustomJSONRenderer):
    object_label = 'customcode'
    pagination_object_label = 'customcode'
    pagination_count_label = 'customcodeCount'

class CommentJSONRenderer(CustomJSONRenderer):    
    object_label = 'comment'
    pagination_object_label = 'comment'
    pagination_count_label = 'commentsCount'    
