from django.conf.urls import include, url
from .views import RegistrationAPIView, UserRetrieveUpdateAPIView, LoginAPIView, UserViewSet

app_name = 'authentication'

user_list = UserViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
user_detail = UserViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

urlpatterns = [
     url('user/register/', RegistrationAPIView.as_view()),
     url('user/login/', LoginAPIView.as_view()),
     url('user/' ,UserRetrieveUpdateAPIView.as_view()),
     url(r'^userlist/$', user_list, name='user_list'),                                       

]