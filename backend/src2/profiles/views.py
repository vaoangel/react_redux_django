from django.shortcuts import render
from rest_framework import generics, mixins, status, viewsets
from rest_framework.exceptions import NotFound
from .models import Profile
from rest_framework.response import Response
from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.permissions import IsAuthenticated, IsAdminUser, IsAuthenticatedOrReadOnly,AllowAny
from .renderers import ProfileJSONRenderer
from .serializers import ProfileSerializer


class ProfileViewSet(viewsets.ModelViewSet):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    lookup_field = 'user_id'
    permission_classes = (AllowAny,)
    # permission_classes = (IsAdminUser,)

class ProfileRetrieveUpdateAPIView(RetrieveUpdateAPIView):
    permission_classes = (AllowAny,)
    queryset = Profile.objects.select_related('user')
    renderer_classes = (ProfileJSONRenderer,)
    serializer_class = ProfileSerializer

    def retrieve(self, request, username, *args, **kwargs):
        try:
            profile = self.queryset.get(user__username=username)
        except Profile.DoesNotExist:
            raise NotFound('A profile with this username does not exist.')

        serializer = self.serializer_class(profile, context={
            'request': request
        })

        return Response(serializer.data, status=status.HTTP_200_OK)
    def update(self, request, *args, **kwargs):
        user_data = request.data.get('profile', {})
        print(request.user.profile)
        serializer_data = {
            'profiles': {
                'bio': user_data.get('bio',),
                'username': user_data.get('username',request.user.username),

                'image': user_data.get('image', ),}
        }
        serializer = self.serializer_class(
        request.user, data=serializer_data, partial=True
        )

        serializer.is_valid(raise_exception=True)
        print(serializer_data)

        serializer.save()

        return Response(serializer.data, status=status.HTTP_200_OK)