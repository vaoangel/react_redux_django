from django.conf.urls import url
from .views import ProfileRetrieveUpdateAPIView,ProfileViewSet

app_name = 'profiles'
profile_list = ProfileViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
profile_detail = ProfileViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})


urlpatterns = [
    url(r'^profilelist/$', profile_list, name='profile_list'),                                      
    url(r'^profiles/(?P<username>\w+)/?$', ProfileRetrieveUpdateAPIView.as_view()),

]