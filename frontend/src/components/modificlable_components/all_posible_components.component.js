import React from "react"

export const All_posible_component = (props) =>{
    const e = React.createElement; 
    console.log(props.custom_style);

    let custom_props = {style:props.custom_style}
    let text = ""
    let type = ""
    switch (props.element_type) {
        case "input":
            type = "input"
            custom_props = {...custom_props, placeholder:"Input Example"}
            break;
        case "button":
            type = "button"
            text = "Button Example"
            break;
        case "input-date":
            type = "input"
            custom_props = {...custom_props, type:"date"}
            break;
        default:
            break;
    }
    

    return (
        e(type,custom_props, text!==""?text:null)
    )


}