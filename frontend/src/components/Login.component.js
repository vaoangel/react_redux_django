import React from 'react';
import {connect} from 'react-redux'
import {all_style_components as sc} from './styler_components/index'
import {AuthApi} from '../router/agent'
import {Link } from "react-router-dom";
const mapDispatchToProps = dispatch => ({
    login: (email, password) =>
    dispatch({ type: "LOGIN", payload: AuthApi.login(email, password)

}),/* agent.Auth.login(email, password)   new Promise((resolve,reject)=>{
        let res = {user:{username:"test",token:"guau un token"}}
        resolve(res);
    }) */
});

const mapStateToProps = state => ({
    style: state.StyleReducer.style.auth_form
});

class Login extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            loginData:{email: "", password:""}
        }
        this.handleChanges = this.handleChanges.bind(this);
        this.validateData = this.validateData.bind(this);



    }
    validateData(){
        const regexp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        var email_val = regexp.test(this.state.loginData.email)
        
        if((this.state.loginData.email === "")||(email_val === false)){
            alert("El email introducido no es válido");
        }else if(this.state.loginData.password === ""){
            alert("El campo de password es requerido");

        }else{
              this.props.login(this.state.loginData.email, this.state.loginData.password)
        }
    }
    handleChanges(data){
        this.setState({loginData:{...this.state.loginData, [data.target.name]: data.target.value}})
    }
   
    render(){

        return(
            <div style={this.props.style.body}>
                <div style={this.props.style.content}>
                    <div style={this.props.style.formBox}>
                        <h2 style={this.props.style.h2}>Welcome!</h2>
                        
                        <div>
                            <span  style={this.props.style.fieldText}> Email</span>
                            <input style={this.props.style.formField} name="email" onChange={this.handleChanges} type="text" placeholder="Username"/>
                            <span  style={this.props.style.fieldText}> Password</span>
                            <input style={this.props.style.formField} name="password" onChange={this.handleChanges} type="password" placeholder="Password"/>
                            <div onClick={this.validateData}>
                                <sc.CustomButton  component_type="auth_form" component_sub_type="formButton" text="Login"/>
                            </div>
                            <span style={this.props.style.formLink} >
                            <Link to="/register">Register</Link>
                            </span>

                        </div>
                        {/* 
                        <a className="form-link" >I lost my password</a> */}
                    </div>
                </div>
            </div>
        )
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Login)