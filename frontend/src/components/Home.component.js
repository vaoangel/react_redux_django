import React from 'react';
import {connect} from 'react-redux'
import {AuthApi} from '../router/agent'
import {Link} from 'react-router-dom'
import './styler_components/styles/component_cards.css'
const mapStateToProps = state => ({
    style: state.StyleReducer.style.home,
    loading:state.CreatorReducer.loading,
    list_components: state.CreatorReducer.data
});
const mapDispatchToProps = dispatch =>({
    onLoad: () => dispatch({type:"FETCH_COMPONENTS",payload:AuthApi.fetchComponents()}),
    success: () => dispatch({type:"FETCH_COMPONENTS_SUCCESS"})
})
class Home extends React.Component {
    state = {components:undefined}
    constructor(props){
        super(props)
        this.props.onLoad()
        this.make_component_list = this.make_component_list.bind(this)
    }
    getSnapshotBeforeUpdate(prevProps, prevState) {
        if (prevProps.list_components != this.props.list_components) {
          const snapshot = this.props.list_components;
          return snapshot
        }    
        return null;
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot != null){
            this.setState({
                components: snapshot.customcode
            })
            this.props.success()
        }
        
    }
    make_component_list(){
        let html = []      
        this.state.components.map((elements) => {
            return html = [...html, 
                <div key={elements.code} className="slide" style={{backgroundImage:"url(https://alca.tv/static/u/522d6a86-0dcf-4554-8b22-7655d65a1f66_opt.png)"}}>
                    <div className="number">Component: {elements.component}</div>
                    <div className="body">
                    {/* <div className="location">Shibuya, Japan</div> */}
                    <div className="headline">{elements.user.username}</div>
                        <Link to={`/details/${elements.id}`}>
                            <div className="link">Ver el componente</div>
                        </Link>
                    </div>
                </div>
            ]
        })
        return html
    }
    render(){  
        console.log(this.props);
                      
        return(
            this.state.components === undefined
            ? <img src="https://i.pinimg.com/originals/3f/2c/97/3f2c979b214d06e9caab8ba8326864f3.gif"></img>
            :<div style={this.props.style}>
                <div id="hero-slides">
                    <div id="slides-cont">
                        <div id="slides">
                        {this.make_component_list()}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Home)