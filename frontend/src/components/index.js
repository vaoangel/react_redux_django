import Layout from "./layout/Layout"
import Home from "./Home.component"
import Login from "./Login.component";
import Profile from './Profile.component'
import Register from './Register.component'
import ComponentCreator from './Component.Creator.component'


export const ProfileComponent = Profile;
export const LayoutComponent = Layout;
export const HomeComponent = Home
export const LoginComponent = Login
export const RegisterComponent = Register
export const CreatorComponent = ComponentCreator