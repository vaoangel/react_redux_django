import React from 'react';
import {Link} from "react-router-dom"
import {all_style_components as sc} from '../styler_components/index'
import { useSelector } from 'react-redux'

export const Header=(props)=> {
  const classes = useSelector(state => state.StyleReducer.style.materialUiCustomStyle)
  const sections = props.sections
  const title = props.title
  return (
      <React.Fragment>
      <div style={classes.toolbar}>
        <h2 style={classes.toolbarTitle}>
        <Link to={`/`} style={classes.toolbarLink}>{title}</Link>
        </h2>
      </div>
      <div style={classes.toolbarSecondary}>
        {sections.map(section => (
          /* console.log(section.dispatch) */
          section.title === "Profile"
          
          ?<a key={section.title} style={classes.toolbarLink} href={section.url}>
            <sc.CustomButton component_type="materialUiCustomStyle" component_sub_type="toolbarLinkButton" text={section.title}/>
          </a>
               


               :<Link  key={section.title} to={`${section.url}`} style={classes.toolbarLink}>
               {section.dispatch === undefined 
                 ?<sc.CustomButton component_type="materialUiCustomStyle" component_sub_type="toolbarLinkButton" text={section.title}/>
                 :<sc.CustomButton 
                         event={{type:"onClick",dispatch:section.dispatch}} 
                         component_type="materialUiCustomStyle" 
                         component_sub_type="toolbarLinkButton" 
                         text={section.title}/>
               }
             </Link>
        ))}
      </div>
    </React.Fragment>
  );
}

export const Footer = () =>{
  const classes = useSelector(state => state.StyleReducer.style.materialUiCustomStyle)
  
  return (
    <React.Fragment>
      <div style={classes.toolbar}>
        <h2  style={classes.toolbarTitle}>
         Footer
        </h2>
      </div>
    </React.Fragment>
  );
}