import React from 'react';
import {Header, Footer} from './header_footer.component'
import {connect} from 'react-redux'
import {all_style_components as sc} from "../styler_components/index"
import { withRouter } from 'react-router-dom';
/* import { setToken } from '../../router/agent' */
/* const Promise = global.Promise; */

const mapStateToProps = state => ({
    weather: state.StyleReducer.weather,
    style: state.StyleReducer.style,
    authUser: state.AuthReducer.currentUser,
    redirectTest:state.AuthReducer.FromLoginTo,
});

const mapDispatchToProps = dispatch => ({
    weatherReport: (payload) =>dispatch({ type: 'TURN_WEATHER', payload }),
    unloadLogin: (payload) => dispatch({type:"LOGIN_SUCCESS", payload}),
    onClickLogout: () => dispatch({ type: "LOGOUT" }),
});
class Layout extends React.Component {
    constructor(props){
        super(props)
        this.testDay = this.testDay.bind(this)
        this.menu = this.menu.bind(this)
    }
    shouldComponentUpdate(nextProps, nextState) {
        if(nextProps.redirectTest !== this.props.redirectTest && nextProps.redirectTest !== null){
            this.props.unloadLogin(this.props.history.push(nextProps.redirectTest))
            return true
        }
        return false
    }
    testDay(){
        this.props.weather === "soleado" ?
            this.props.weatherReport({weather:"noche"})
        :
            this.props.weatherReport({weather:"soleado"})
    }
    menu(){
        return this.props.authUser===null 
            ? [{title:'Login',url:'/login'}]
            : [ {title:'Logout',url:'/',dispatch:this.props.onClickLogout},
                {title:"Profile",url:"http://localhost:8081/#/profile/"+this.props.authUser.username+"&"+this.props.authUser.token},
                {title:"Component Creator",url:"/creator"}]
  
    }
    render(){
        return(
            <div>
                <Header title={"CodeCreator"} sections={this.menu()}></Header>
                <div>
                  {this.props.children}
                    <div onClick={this.testDay}>
                        {<sc.CustomButton 
                                component_type="weather_button"
                                text="Test"
                                type="standar">
                        </sc.CustomButton>}
                    </div>
                </div>
                <Footer></Footer>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Layout))
