import React from 'react';
import {connect} from 'react-redux'
import {all_style_components as sc} from './styler_components/index'
import {AuthApi} from '../router/agent'


const mapStateToProps = state => ({
    style: state.StyleReducer.style.auth_form
});


const mapDispatchToProps = dispatch =>({
    register:(email, password, username) =>
        dispatch({type: "REGISTER", payload: AuthApi.register(email,password,username)})
})

class Register extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            registerData:{email: "", password:"", username:"", password2: ""}
        }
        this.handleChanges = this.handleChanges.bind(this);
        this.validateData = this.validateData.bind(this);
    }
    validateData(){
        if(this.state.registerData.password === ""){
            alert("El campo de password es requerido");

        }else if(this.state.registerData.username === ""){
            alert("El Usuario es requerido");

        }else if(this.state.registerData.password !== this.state.registerData.password2){
            alert("Las contraseñas no coinciden")

        }else{
            console.log("entra");
            console.log(this.setState.registerData);
            
            this.props.register(this.state.registerData.email, this.state.registerData.password, this.state.registerData.username)
        }
    }
    handleChanges(data){
        this.setState({registerData:{...this.state.registerData, [data.target.name]: data.target.value}})
    }
    render(){

        return(
            
            <div style={this.props.style.body}>
            <div style={this.props.style.content}>
                <div style={this.props.style.formBox}>
                    <h2 style={this.props.style.h2}>Welcome!</h2>
                    
                    <div>
                        <span  style={this.props.style.fieldText}> Username</span>
                        <input style={this.props.style.formField} name="username" onChange={this.handleChanges} type="text" placeholder="Username"/>
                        <span  style={this.props.style.fieldText}> Email</span>
                        <input style={this.props.style.formField} name="email" onChange={this.handleChanges} type="text" placeholder="Email"/>
                        <span  style={this.props.style.fieldText}> Password</span>
                        <input style={this.props.style.formField} name="password" onChange={this.handleChanges} type="password" placeholder="Password"/>
                        <span  style={this.props.style.fieldText}> Repeat the password</span>
                        <input style={this.props.style.formField} name="password2" onChange={this.handleChanges} type="password" placeholder="Password"/>
                        <div onClick={()=>this.validateData()   }>
                            <sc.CustomButton component_type="auth_form" component_sub_type="formButton" text="Register"/>
                        </div>
                    </div>
           
                     {/* <a className="form-link toleft" >Register</a>
                    <a className="form-link" >I lost my password</a>   */}
                </div>
            </div>
        </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps )(Register)