import React from 'react';
import {connect} from 'react-redux'
import {All_posible_component} from './modificlable_components/all_posible_components.component'
import ColorSwitcher from './styler_components/colorSwitcher.component'
import {all_style_components as asc} from './styler_components/index'
import {AuthApi} from '../router/agent'
const mapStateToProps = state => ({
    creator_style: state.StyleReducer.style.creator_component,
    select_style: state.StyleReducer.style.custom_select
});
const mapDispatchToProps = dispatch => ({
    createComponent: (payload) => dispatch({ type: "CREATE", payload: payload }), /* Acabar el post del componente */
});
class ComponentCreator extends React.Component {
    no_editor_text =["input-radio","input-checkbox"]
    
    constructor(props){
        super(props)
        this.state = {
            show_border_options:false,
            show_component_result:false,
            created_element_style:{
              
            },
            actual_element_type:"input",
            text_editor:true
        }
        this.elementChange = this.elementChange.bind(this)
        this.make_options = this.make_options.bind(this)
        this.styleChanges = this.styleChanges.bind(this)
    }
    elementChange(newValue){
        let editor_text = !this.no_editor_text.includes(newValue.target.value) 

 
        this.setState({...this.state, 
                        actual_element_type: newValue.target.value,
                        text_editor:editor_text 
                        })
        
    }
    styleChanges(e,option){
        if(option==="borderBool"){
            return e.target.checked
            ? this.setState({...this.state,
                show_border_options:e.target.checked,
                created_element_style:{...this.state.created_element_style,
                                                   borderColor:"red",
                                                   borderStyle:"solid",
                                                   borderRadius:"0px"}})
            : this.setState({...this.state,
                show_border_options:e.target.checked,
                created_element_style:{...this.state.created_element_style,
                                                   borderColor:"",
                                                   borderStyle:"",
                                                   borderRadius:""}})
        }else if (option==="borderRadius" || option==="borderWidth") {
            return this.setState({...this.state,
                created_element_style:{...this.state.created_element_style,
                                                   [option]:e.target.value +"px"}})
        }
       
         return this.setState({...this.state,
                created_element_style:{...this.state.created_element_style,
                                                   [option]:e.target.value}})
        
        
            
    }
    make_options(){
        let html = []
        let list_style = this.props.creator_style.animatedListLi
        let options = {
            TextColor:<li key="textColor" style={list_style} 
                        onChange={(e)=>this.styleChanges(e,"color")}>Text Color: <ColorSwitcher></ColorSwitcher></li>,
            BackgroundColor: <li key="bcolor" style={list_style}
                                onChange={(e)=>this.styleChanges(e,"backgroundColor")}>Background Color: <ColorSwitcher></ColorSwitcher></li>,
            BorderBool:   <li key="border" style={list_style}>
                           Borders: <input type="checkbox" onChange={(e)=>this.styleChanges(e,"borderBool")}></input></li>,
            BorderColor:  <li  key="borcolor" style={list_style}
                                onChange={(e)=>this.styleChanges(e,"borderColor")}>Border Color: <ColorSwitcher></ColorSwitcher></li>,
            BordrLine:    <li  key="borline" style={list_style}>Border Size: 
                            <input onChange={(e)=>this.styleChanges(e,"borderWidth")} type="range" min="0" max="100" step="1"></input></li>,
            borderRadius: <li  key="borradius" style={list_style}>Border Radius: 
                            <input onChange={(e)=>this.styleChanges(e,"borderRadius")} type="range" min="0" max="100" step="1"></input></li>
            
        }
        this.state.text_editor 
            ? html.push(
                [options["TextColor"],options["BackgroundColor"],options["BorderBool"]]
            )
            : html.push(
                [options["BackgroundColor"],options["BorderBool"]]
            )
        this.state.show_border_options
            ? html.push([options["BorderColor"],options["BordrLine"],options["borderRadius"]])
            : html.push("")
        return html
    }
    render(){
        return(
            <div>
                {this.state.show_component_result 
                ? <asc.DetailComponent component_type={this.state.actual_element_type} 
                                   component_style={this.state.created_element_style}></asc.DetailComponent>
                :<div>
                    <div style={this.props.select_style.customSelect}>
                        <span>Select the component Type</span>
                        <select  onChange={this.elementChange} style={this.props.select_style.customSelectSelectArea}>
                            <option value="input">Input Text</option>
                            <option value="input-date">Input Date</option>
                            <option value="button">Button</option>
                        </select>
                        <button 
                            onClick={()=>{
                                this.setState({...this.state, show_component_result:true});
                                this.props.createComponent(
                                        AuthApi.createComponent({
                                                                type:this.state.actual_element_type,
                                                                style:this.state.created_element_style
                                                            }))
                                }}>Submit</button>
                    </div>
                    <div style={this.props.creator_style.container}> 
                        <div style={this.props.creator_style.options}>
                            <ul style={this.props.creator_style.animatedList}>
                                {this.make_options(this.state)}
                            </ul>
                        </div>
                        
                        <div style={this.props.creator_style.result}>
                            <All_posible_component 
                                element_type={
                                    this.state.actual_element_type !=="" ? this.state.actual_element_type : "input"
                                } 
                                custom_style = {this.state.created_element_style}
                            ></All_posible_component>
                        </div>
                    </div>
                </div>
                }
                
            </div>
        )
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ComponentCreator)