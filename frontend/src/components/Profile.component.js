import React from 'react';
import {connect} from 'react-redux'
/* import {all_style_components as sc} from './styler_components/index' */
import {AuthApi} from '../router/agent'

const mapStateToProps = state => ({
    style: state.StyleReducer.style.profile,
    currentUser: state.AuthReducer.currentUser,
    profileData: state.ProfileReducer
});

const mapDispatchToProps = dispatch =>({
    getProfile:(username) =>
    dispatch({type: "GET_PROFILE", payload: AuthApi.getProfile(username)}),

    loadedProfile:()=>
    dispatch({type:"PROFILE_LOAD_SUCCESS"})
})

class Profile extends React.Component{

    constructor(props){
        super(props);
        this.props.getProfile(this.props.currentUser.username)

    }
    render(){
        console.log(this.props.profileData);
        
        return(
            <div style={this.props.style.body}>
                <div style={this.props.style.card}>
                   
                    <h1>Cristian Pardo</h1>
                    <p style={this.props.style.title}> Sensillo</p>
                    <p>SensilloV2</p>
                    <i style={this.props.style.a}></i>

                   
                    <p><button style={this.props.style.button}>Bondia</button></p>
                </div>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile)