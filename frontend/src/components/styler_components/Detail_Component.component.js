import React from 'react'
import { connect } from 'react-redux';
import {AuthApi} from '../../router/agent'
import DETAILED_CREATED_COMPONENT from './Detail_Created_Component.component'
const mapStateToProps = state => ({
    style: state.StyleReducer.style.home,
    component: state.CreatorReducer.data
});
const mapDispatchToProps = dispatch =>({
    onLoad: (id) => dispatch({type:"FETCH_COMPONENTS",payload:AuthApi.fetchComponent(id)}),
    success: () => dispatch({type:"FETCH_COMPONENTS_SUCCESS"})
})

class DETAIL_COMPONENT_CONTAINER extends React.Component {
    state = {component:undefined}
    constructor(props){
        super(props)
        this.props.onLoad(this.props.match.params.component)
    }
    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.component !== prevState.component) {
            return {
                component:nextProps.component.customcode,
                id: nextProps.component.customcode.component,
                style:nextProps.component.customcode.code
            };
          }
      
          // Return null if the state hasn't changed
          return null;
      }
      render(){
            console.log(this.state);
            
      return this.state.id !== undefined ?      
            <DETAILED_CREATED_COMPONENT 
                        component_style={JSON.parse(this.state.style)}
                        component_type={this.state.id}>
            </DETAILED_CREATED_COMPONENT>
            
            :"loading"
      }


}

export default connect(mapStateToProps,mapDispatchToProps)(DETAIL_COMPONENT_CONTAINER)

