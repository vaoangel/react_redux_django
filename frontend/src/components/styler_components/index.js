import {CustomizableButton} from './button.component'
import DETAILED_CREATED_COMPONENT from './Detail_Created_Component.component'
import {weather_button,
        home,
        materialUiCustomStyle,
        auth_form, profile, creator_component,custom_select} from './styles/day_styles'
import {weather_button as Nbutton_Standar,
        home as Nhome,
        materialUiCustomStyle as NmaterialUiCustomStyle,
        auth_form as Nauth_form, profile as Nprofile,
        creator_component as Ncreator_component,custom_select as Ncustom_select} from './styles/nigth_styles'

export const all_style_components = {
    CustomButton: CustomizableButton,
    DetailComponent:DETAILED_CREATED_COMPONENT
}

export const all_styler_types = {
    day_styles:{
        weather_button:weather_button,
        home:home,
        materialUiCustomStyle:materialUiCustomStyle,
        auth_form:auth_form,
        profile:profile,
        creator_component:creator_component,
        custom_select:custom_select
    },
    nigth_styles:{
        weather_button:Nbutton_Standar,
        home:Nhome,
        materialUiCustomStyle:NmaterialUiCustomStyle,
        auth_form:Nauth_form,
        profile:Nprofile,
        creator_component:Ncreator_component,
        custom_select:Ncustom_select
    }
}