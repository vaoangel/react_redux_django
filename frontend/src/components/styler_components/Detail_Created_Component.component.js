import React from "react"
import {useSelector} from "react-redux"
import AceEditor from "react-ace";
import "ace-builds/src-noconflict/mode-jsx";
import 'ace-builds/src-noconflict/mode-css'
import 'ace-builds/src-noconflict/snippets/css'
import 'ace-builds/src-noconflict/theme-monokai'
import 'ace-builds/src-noconflict/theme-github'
import {All_posible_component} from '../modificlable_components/all_posible_components.component'
const reactToCSS = require('react-style-object-to-css')

const DETAILED_CREATED_COMPONENT = (props)=>{
    let customWeather = useSelector(state => state.StyleReducer.style["creator_component"])

    return (
        <div style={customWeather.container}> 
        <div style={customWeather.options}>
            <AceEditor 
                placeholder="Placeholder Text"
                mode="css"
                theme="monokai"
                name="blah2"
                fontSize={14}
                showPrintMargin={true}
                showGutter={true}
                readOnly = {true}
                highlightActiveLine={true}
                value={reactToCSS(props.component_style)}
                setOptions={{
                    wrap:true,
                    enableSnippets: false,
                    showLineNumbers: true,
                    tabSize: 1,
            }}/>
        </div>
        
        <div style={customWeather.result}>
            <All_posible_component 
                element_type={props.component_type} 
                custom_style = {props.component_style}
            ></All_posible_component>
        </div>
    </div>
    )
}

export default DETAILED_CREATED_COMPONENT