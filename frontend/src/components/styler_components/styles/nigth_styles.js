export const materialUiCustomStyle = {
  toolbar: {
    color:"White",
    /* borderBottom: `1px solid #c8c8c8`, */
    backgroundColor: "#282248",
    minHeight: "50px"
  },
  toolbarTitle: {
    flex: "1",
    color:"inherit",
    align:"center",
    fontFamily: "Arial",
    margin: "0px 0px 0px 45%"
  },
  toolbarSecondary: {
    justifyContent: 'space-between',
    overflowX: 'auto',
    /* borderBottom: `1px solid #c8c8c8`, */
    backgroundColor: "#282248"
  },
  toolbarLink: {
    padding: "8px",
    flexShrink: "0",
    color:"inherit",
    textDecoration: "none",
    textTransform: "capitalize",
    fontFamily: "Arial"
  },
  toolbarLinkButton: {
    backgroundColor:"#141124",
    color:"White",
    border: "none",
    padding: "15px 32px",
    textAlign: "center",
    textDecoration: "none",
    display: "inline-block",
    fontSize: "16px",
    boxShadow: "rgba(255, 255, 255, 0.2) 0px 8px 16px 0px, rgba(255, 242, 242, 0.19) 0px 6px 20px 0px",
    textTransform:"capitalize",
    margin: "8px",
    cursor:"pointer"
  }
}
export const weather_button ={
    backgroundColor:"#00b2db",
    color:"White",
    position: "fixed",
    right: "0",
    top: "0",
    bottom: "0",
    height: "3.5rem",
    margin: "auto",
    zIndex: "10",
    fontSize: ".65rem",
    textTransform: "uppercase",
    lineHeight: "1.5",
    borderRadius: ".3rem 0 0 .3rem",
}
export const home ={
    color:"White",
    backgroundColor:"#18142f",
    padding: "8px",
    minHeight: "500px"
}
export const profile ={
  body:{
    display: "flex",
    margin: "0",
    height: "75vh",
    background: "#18142f",
    fontFamily: "Open Sans, sans-serif",
    color: "#fff",
    
  },
  h2:{
    marginTop: "0px",
    marginBottom: "16px",
    textAlign: "center",
    fontFamily: "Noto Serif, serif",
    fontSize: "32px",
    /*font-weight: normal,*/
    color:"#FFF",
  },

  card:{
    boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2)",
    maxWidth: "100%",
    margin: "auto",
    textAlign: "center",
    backgroundColor: "purple",

  },

  title:{
    border: "none",
    outline: "0",
    display: "inline-block",
    padding: "8px",
    color: "white",
    backgroundColor: "purple",
    textAlign: "center",
    cursor: "pointer",
    width: "80%",
    fontSize: "18px",
  },

  a:{
    textDecoration: "none",
    fontSize: "22px",
    color: "black"
  },
  button:{
    backgroundColor: "black",
    border: "none",
    color: "white",
    padding: "15px 32px",
    textAlign: "center",
    textDecoration:"none",
    display: "inline-block",
    fontSize: "16px"

  }
  
}
export const auth_form ={
  body:{
    display: "flex",
    margin: "0",
    height: "75vh",
    background: "#18142f",
    fontFamily: "Open Sans, sans-serif",
  },
  h2:{
    marginTop: "0px",
    marginBottom: "16px",
    textAlign: "center",
    fontFamily: "Noto Serif, serif",
    fontSize: "32px",
    /*font-weight: normal,*/
    color:"#FFF",
  },
  
  content: {
    margin: "auto",
  },
  
  formBox:{
    padding: "48px 16px",
    width: "300px",
    /* boxShadow: "0 0 10px #e2e2e2", */
    background: "#282248",
    overflow: "hidden",
  },
  
  formField:{
    marginBottom: "16px",
    width: "95%",
    padding: "7px",
    fontSize: "16px",
    border: "2px solid #e2e2e2",
    transition: "border-color 250ms",
  },
    
  formButton :{
    padding: "8px",
    width: "100%",
    cursor: "pointer",
    border: "2px solid #333333",
    background: "#00b2db",
    fontSize: "16px",
    color: "#FFF",
    transition: "250ms",
    fontWeight: "bold",
  },
   
  formLink :{
    marginTop: "16px",
    display: "block",
    float: "right",
    fontSize: "14px",
    color: "#A0A0A0",
    textDecoration: "none",
    borderBottom: "2px solid white",
    transition: "border-color 250ms, color 250ms",
  },
  
  formLinkLoleft: {
    float: "left",
  },
    
  fieldText: {
    color:"#FFF",
    fontWeight: "bold",
    fontSize: "14px",
    marginBottom: "4px",
    display: "block",
  }

}
export const creator_component = {
  container:{
    color: "Withe",
    backgroundColor:"#18142f",
    padding: "8px",
    display:"flex",
    flexFlow:"row nowrap",
    minHeight:"500px"
  },
  options:{
    width:"40%"
  },
  animatedList: {
    width: "100%",
    padding: "0",
    margin: "8px",
    listStyle: "none",
    fontFamily: "arial",
    display:"flex",
    flexFlow:"row wrap"
  },
    
  animatedListLi: {
    backgroundColor: "#141124",
    marginBottom: "5px",
    color: "#fff",
    padding: "1em",
    width: "170px",
    
  },
  result:{
    width: "60%",
    display: "flex",
    alignItems: "center",
    flexFlow: "column",
    alignSelf: "center",
  }
}
export const custom_select = {
  customSelect:{
    backgroundColor: "#18142f",
    position: "relative",
    textAlign:"center",
    color:"#fff"
  },
  customSelectSelectArea:{
    position: "relative",
    zIndex: "2",
    width: "20%",
    height: "100%",
    padding: "10px",
    backgroundColor: "#141124",
    color:"#fff",
    textAlign:"center",
    margin:"8px",
    appearance: "none",
    border:"none"
  }
}