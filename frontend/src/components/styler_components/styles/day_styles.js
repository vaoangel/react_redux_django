export const materialUiCustomStyle = {
    toolbar: {
      color:"Black",
      borderBottom: `1px solid #c8c8c8`,
      backgroundColor: "rgba(255, 255, 255, 0.91)",
      minHeight: "50px"
    },
    toolbarTitle: {
      flex: "1",
      color:"inherit",
      align:"center",
      fontFamily: "Arial",
      margin: "0px 0px 0px 45%"
    },
    toolbarSecondary: {
      justifyContent: 'space-between',
      overflowX: 'auto',
      backgroundColor: "#e8e8e8e8",
      borderBottom: `1px solid #c8c8c8`,
    },
    toolbarLink: {
      padding: "8px",
      flexShrink: "0",
      color:"inherit",
      textDecoration: "none",
      textTransform: "capitalize",
      fontFamily: "Arial"
    },
    toolbarLinkButton: {
      backgroundColor:"#e8e8e8e8",
      color:"Black",
      border: "none",
      padding: "15px 32px",
      textAlign: "center",
      textDecoration: "none",
      display: "inline-block",
      fontSize: "16px",
      boxShadow: "0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19)",
      textTransform:"capitalize",
      margin: "8px",
      cursor:"pointer"
    }
}
export const weather_button ={
    backgroundColor:"#000",
    color:"White",
    position: "fixed",
    right: "0",
    top: "0",
    bottom: "0",
    height: "3.5rem",
    margin: "auto",
    zIndex: "10",
    fontSize: ".65rem",
    textTransform: "uppercase",
    lineHeight: "1.5",
    borderRadius: ".3rem 0 0 .3rem",
    cursor:"pointer"
}
export const home ={
  color: "Black",
  backgroundColor:"#FFF",
  padding: "8px",
  minHeight: "500px"
}
export const creator_component = {
  container:{
    color: "Black",
    backgroundColor:"rgb(234, 234, 234)",
    padding: "8px",
    display:"flex",
    flexFlow:"row nowrap",
    minHeight:"500px"
  },
  options:{
    width:"40%"
  },
  animatedList: {
    width: "100%",
    padding: "0",
    margin: "8px",
    listStyle: "none",
    fontFamily: "arial",
    display:"flex",
    flexFlow:"row wrap"
  },
    
  animatedListLi: {
    backgroundColor: "#e8e8e8",
    marginBottom: "5px",
    color: "#000",
    padding: "1em",
    width: "170px",
  },
  result:{
    width: "60%",
    display: "flex",
    alignItems: "center",
    flexFlow: "column",
    alignSelf: "center",
  }
}
export const custom_select = {
  customSelect:{
    backgroundColor: "#e8e8e8",
    position: "relative",
    textAlign:"center"
  },
  customSelectSelectArea:{
    position: "relative",
    zIndex: "2",
    width: "20%",
    height: "100%",
    padding: "10px",
    backgroundColor: "#e8e8e8",
    color:"#000",
    textAlign:"center",
    margin:"8px",
    appearance: "none",
    border:"none"
  }
}
export const profile ={
  body:{
    display: "flex",
    margin: "0",
    height: "75vh",
    background: "lightgrey",
    fontFamily: "Open Sans, sans-serif",
  },
  h2:{
    marginTop: "0px",
    marginBottom: "16px",
    textAlign: "center",
    fontFamily: "Noto Serif, serif",
    fontSize: "32px",
    /*font-weight: normal,*/
    color:"#FFF",
  },
  card:{
    boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2)",
    maxWidth: "300px",
    margin: "auto",
    textAlign: "center",
  },

  title:{
    border: "none",
    outline: "0",
    display: "inline-block",
    padding: "8px",
    color: "white",
    backgroundColor: "#000",
    textAlign: "center",
    cursor: "pointer",
    width: "90%",
    fontSize: "18px",
  },

  a:{
    textDecoration: "none",
    fontSize: "22px",
    color: "grey"
  },
  button:{
    backgroundColor: "grey",
    border: "none",
    color: "black",
    padding: "15px 32px",
    textAlign: "center",
    textDecoration:"none",
    display: "inline-block",
    fontSize: "16px"

  }
}
export const auth_form ={
  body:{
    display: "flex",
    margin: "0",
    height: "75vh",
    background: "rgb(234, 234, 234) none repeat scroll 0% 0%",
    fontFamily: "Open Sans, sans-serif",
  },
  h2:{
    marginTop: "0px",
    marginBottom: "16px",
    textAlign: "center",
    fontFamily: "Noto Serif, serif",
    fontSize: "32px",
    /*font-weight: normal,*/
    color:" #333333",
  },
  content: {
    margin: "auto",
  },
  
  formBox:{
    padding: "48px 16px",
    width: "300px",
    boxShadow: "0 0 10px #e2e2e2",
    background: "white",
    overflow: "hidden",
  },
  
  formField:{
    marginBottom: "16px",
    width: "95%",
    padding: "7px",
    fontSize: "16px",
    border: "2px solid #e2e2e2",
    transition: "border-color 250ms",
  },
    
  formButton :{
    padding: "8px",
    width: "100%",
    cursor: "pointer",
    border: "2px solid #333333",
    background: "#333333",
    fontSize: "16px",
    color: "#A3A3A3",
    transition: "250ms",
  },
   
  formLink :{
    marginTop: "16px",
    display: "block",
    float: "right",
    fontSize: "14px",
    color: "#A0A0A0",
    textDecoration: "none",
    borderBottom: "2px solid white",
    transition: "border-color 250ms, color 250ms",
  },
  
  formLinkLoleft: {
    float: "left",
  },
    
  fieldText: {
    fontWeight: "bold",
    fontSize: "14px",
    marginBottom: "4px",
    display: "block",
  }

}