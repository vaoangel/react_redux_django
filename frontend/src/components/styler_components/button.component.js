import React from "react"
import { useSelector } from 'react-redux'

export const CustomizableButton = (props) =>{
    const e = React.createElement; /* elemento html */

    /* CSS del modo diurno y nocturno. Carga mediante Hooks el state proporcionado por el reducer
       de estilos. Pasado por el props.component_type selectionamos del objeto style porporcionado por el reducer
       un estilo para nestro boton*/
    let customWeather = useSelector(state => state.StyleReducer.style[props.component_type])
    /* si existe un props.component_sub_type, seleccionaremos del objeto obtenido por props.component_type un estilo de
    su interior */
    if(props.component_sub_type !== undefined){
        customWeather = customWeather[props.component_sub_type]
    }

    /* Creamos el objeto de los props del elemento. Por defecto el objeto solo tendrá estilo */
    let custom_props = {style:customWeather}
    /* En el caso de que por props pasemos un evento al componente lo añadiremos al objeto */
    if(props.event!==undefined){
        custom_props[props.event.type]=props.event.dispatch;

    }

    return (
        e("button",custom_props,props.text)
    )


}