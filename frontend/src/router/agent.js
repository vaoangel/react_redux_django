import axios from 'axios'
let token = null
let API = axios.create({
    baseURL: `http://0.0.0.0:8000/`,
    headers: {
        'Authorization': token!==null ? `Token ${token}` : "",
    },
});
const setToken = (new_token) => {
    API = axios.create({
        baseURL: `http://0.0.0.0:8000/`,
        headers: {
            'Authorization': new_token!==null ? `Token ${new_token}` : "",
        },
    })
}

const request ={
    // del:  url => API.delete(url).then().catch(),
    post:(url, body) => API.post(url,body).then(function (data) {       
        return data.data
    }).catch((e)=>{
        console.log(e)
        console.log(url);
    }),
    get:  url => API.get(url).then(function (data){
        return data
    }).catch(),
    patch: (url,body) => API.patch(url,body).then((data)=>{
        console.log(data);
        
        return data
    })
}

const AuthApi ={
    login: (email, password) =>
            request.post('api/user/login/', { "user": { "email":email, "password":password } }),
    register: (email, password,username) => 
              request.post('api/user/register/', {"user": {"username": username, "email":email, "password":password}}),
    getProfile: (username) =>
                request.get('profile/profiles/'+username),
    createComponent :(component)=>
                request.post('customcode/',{customcode:{code:JSON.stringify(component.style),component:component.type}}),
    fetchComponents :()=>
                request.get('customcode/'),
    fetchComponent :(id)=>
                request.patch(`customcode/`,{customcode:{id:id}})

}
export {
    AuthApi,
    setToken
  };