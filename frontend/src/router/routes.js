import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { useSelector } from 'react-redux'
import {HomeComponent,
        LoginComponent, 
        ProfileComponent, 
        RegisterComponent,
        CreatorComponent} from '../components/index';
import DETAIL_COMPONENT_CONTAINER from '../components/styler_components/Detail_Component.component'

/* Obtiene por props:
    options= {elementType:"Private"||"restricted"}
    component={(Componente a cargar en la ruta)}
    rest= (los demas props como el path y si es una ruta exacta)*/
const CustomRoutes = ({options:options,component: Component, ...rest})=>{
    let isAuth = useSelector(state => state.AuthReducer.currentUser)
    let routeTypes = {
        private:  <Route {...rest} render={props => (isAuth 
                                                      ?<Component {...props} /> 
                                                      : <Redirect to={`/${options.redirect}`}/>)} />,
        restricted: <Route {...rest} render={props => (isAuth 
                                                        ?<Redirect to={`/${options.redirect}`} /> 
                                                        :<Component {...props} />)} />
    } 
    return routeTypes[options.elementType]
}
const BaseRouter = () =>{
 return(
    <div>

        <Route exact path='/' component={HomeComponent}/>
        <Route exact path='/details/:component' component={DETAIL_COMPONENT_CONTAINER}/>

        {/* <CustomRoutes options={{elementType:"private",redirect:"login"}} component={ProfileComponent} exact path="/profile"  /> */}
        <CustomRoutes options={{elementType:"private",redirect:"login"}} component={CreatorComponent} exact path="/creator"  />

        <CustomRoutes options={{elementType:"restricted",redirect:""}} exact path='/login' component={LoginComponent}/>
        <CustomRoutes options={{elementType:"restricted",redirect:""}} exact path='/register' component={RegisterComponent}/>
    </div>
 )
}
export default BaseRouter



