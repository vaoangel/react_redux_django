import ReactDOM from 'react-dom';
import React from 'react';
import {Provider} from 'react-redux'
import { HashRouter as Router } from 'react-router-dom';
import {LayoutComponent} from './components/index';
import BaseRouter from './router/routes'
import store from './store/store'


const app = (
    <Provider store={store}>
        <Router>
            <LayoutComponent>
                <BaseRouter></BaseRouter>
            </LayoutComponent>
        </Router>
    </Provider>
)

ReactDOM.render((
    app
), document.getElementById('root'));


