import ActionTypes from './ActionTypes'
import {setToken} from '../router/agent'
  const promiseMiddleware = store => next => action => {
    /* console.log(action) */
    if (isPromise(action.payload)) {
      
      store.dispatch({ type: ActionTypes.ASYNC_START, subtype: action.type });

      const currentView = store.getState().viewChangeCounter;
      
      const skipTracking = action.skipTracking;

      action.payload.then(
        res => {
          const currentState = store.getState()
          if (!skipTracking && currentState.viewChangeCounter !== currentView) {
            return
          }
          action.payload = res;
          store.dispatch({ type: ActionTypes.ASYNC_END, promise: action.payload });
          store.dispatch(action);
        },
        error => {
          const currentState = store.getState()
          if (!skipTracking && currentState.viewChangeCounter !== currentView) {
            return
          }
          console.log('ERROR', error);
          action.error = true;
          action.payload = error.response.body;
          if (!action.skipTracking) {
            store.dispatch({ type: ActionTypes.ASYNC_END, promise: action.payload });
          }
          store.dispatch(action);
        }
      );
  
      return;
    } 
    
    if (action.type === ActionTypes.AUTH_TYPES.REGISTER || action.type === ActionTypes.AUTH_TYPES.LOGIN) {
      console.log(action)
      if (!action.error) {
        window.localStorage.setItem('jwt', action.payload.user.token);
        window.localStorage.setItem('user', JSON.stringify(action.payload.user));
        setToken(action.payload.user.token);
      }
    } else if (action.type === ActionTypes.AUTH_TYPES.LOGOUT) {
      window.localStorage.removeItem('jwt');
      window.localStorage.removeItem('user');
      setToken(null);
    }
    next(action);
  };
function isPromise(v) {
  return v && typeof v.then === 'function';
}

export {
  promiseMiddleware
};