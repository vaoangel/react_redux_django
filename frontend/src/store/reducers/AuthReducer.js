import ActionTypes from '../ActionTypes'

const defaultState ={
    FromLoginTo:null,
    token: window.localStorage.getItem('jwt')!==null ? window.localStorage.getItem('jwt') : null,
    currentUser: window.localStorage.getItem('user')!==null ? JSON.parse(window.localStorage.getItem('user')) : null,
    loading:false,
    error:null,
}

const auth =(state,action) =>{
    // console.log(action)
    return {
        ...state,
        FromLoginTo: '/',
        token: action.error ? null : action.payload.user.token,
        currentUser: action.error ? null : action.payload.user
      }
}
const auth_success=(state)=>{
    return {
        ...state,
        FromLoginTo:null,
      }
}
const logout =(state,action)=>{
    return { ...state, 
                token: null,
                currentUser: null,
                FromLoginTo:'/' };
}
const Action = ActionTypes.AUTH_TYPES
export default (state = defaultState, action)=>{
    switch (action.type) {
        case Action.LOGIN:
        case Action.REGISTER:
            return auth(state,action);
        case Action.LOGIN_SUCCESS:
        case Action.REGISTER_SUCCESS:
            return auth_success(state,action)
        case Action.LOGOUT:
            return logout(state,action)
        case ActionTypes.ASYNC_START:
            if (action.subtype === Action.LOGIN || action.subtype === Action.REGISTER) {
                return { ...state, loading: true };
            }
            return state
        
        default:
            return state;
    }
}
