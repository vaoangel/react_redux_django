import ActionTypes from '../ActionTypes'

const defaultState ={
    loading:true,
    error:null,
    data:undefined
}

const Action = ActionTypes.COMPONENT_CREATOR
export default (state = defaultState, action)=>{
    switch (action.type) {
        case Action.CREATE:
            return {...state}
        case Action.CREATE_SUCCESS:
            return {...state,loading:false}
        case ActionTypes.ASYNC_START:
            return state
        case Action.FETCH_COMPONENTS:
            console.log(action);
            return {...state,data:action.payload.data}
        case Action.FETCH_COMPONENTS_SUCCESS:
            console.log(action.payload);
            return {...state,loading:false}
        
        default:
            return state;
    }
}
