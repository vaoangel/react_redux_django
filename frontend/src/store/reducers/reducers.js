import { combineReducers } from 'redux';
import StyleReducer from './StyleReducer'
import AuthReducer from './AuthReducer'
import ProfileReducer from  './ProfileReducer'
import CreatorReducer from  './ComponentCreatorReducer'


export default combineReducers({
    StyleReducer,
    ProfileReducer,
    AuthReducer,
    CreatorReducer
    
});