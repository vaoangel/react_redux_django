import ActionTypes from '../ActionTypes'

const defaultState ={
    username: null,
    bio: null,
    image: null
}

const get_profile = (state, action)=>{
    
    return {
        ...state,
        username:action.payload.data.user.username,
        bio: action.payload.data.user.bio,
        image: action.payload.data.user.image,
    }
}
const Action = ActionTypes.AUTH_TYPES
export default (state = defaultState, action)=>{
    switch (action.type) {
        case Action.GET_PROFILE:
            return get_profile(state,action)
        case Action.PROFILE_LOAD_SUCCESS:
            return {...state}

        case ActionTypes.ASYNC_START:
            
            if(action.subtype === Action.GET_PROFILE){
                return {...state}
            }   
                return state
        default:
            return state;
    }
}
