import {all_styler_types} from "../../components/styler_components/index";

function styleReport(){
    switch (window.localStorage.getItem("weather")) {
        case "soleado": return all_styler_types.day_styles
        case "noche": return all_styler_types.nigth_styles
        default:return all_styler_types.day_styles
    }
}
const defaultState ={ 
    weather: window.localStorage.getItem("weather")!==null ?
             window.localStorage.getItem("weather") :
            "soleado",
    style: styleReport()
}


export default function(state = defaultState, action) {
    //  console.log(action.type)
    switch(action.type) {
        case 'TURN_WEATHER':
            window.localStorage.setItem("weather",action.payload.weather)
            return { 
                ...state, 
                weather: action.payload.weather,
                style:   action.payload.weather ==="soleado" ? all_styler_types.day_styles : all_styler_types.nigth_styles
            
            };
        default:
            return state
    }
};